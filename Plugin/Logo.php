<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Assaka\Interceptor\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Asset\Repository;

class PluginLogo
{

    protected $assetRepo;
    protected $request;

    public function __construct(
        Repository $assetRepo,
        RequestInterface $request
    ) {
        $this->assetRepo = $assetRepo;
        $this->request = $request;
    }

    public function getViewFileUrl($fileId, array $params = [])
    {
        $params = array_merge(['_secure' => $this->request->isSecure()], $params);
        return $this->assetRepo->getUrlWithParams($fileId, $params);
    }

    public function afterGetLogoSrc(\Magento\Theme\Block\Html\Header\Logo $value)
    {
        $url = $this->getViewFileUrl('Assaka_Interceptor::images/primolens_logo.png');
        return $url;

    }

}